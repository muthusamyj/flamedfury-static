---
title: Poodles In the Garden
date: 2020-02-09T08:12:22.958Z
draft: false
---
I use the term garden loosely as it leaves a lot to be desired.

![](/images/uploads/006F26D4-ADC2-4087-BC00-3FB30EFF26F4.jpeg)

At least when the sun is out the dogs enjoy being out there.

![](/images/uploads/955634AD-EFE4-450D-AE91-14ABD75CB0F3.jpeg)

At least they can’t wreck it I guess.
